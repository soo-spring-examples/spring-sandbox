package ua.soo.springsandbox.postgres;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("test-entity")
@AllArgsConstructor
public class TestEntityResource {

    private final TestEntityService service;

    @GetMapping("d/{id}")
    public Collection<TestEntity> getByDefault(@PathVariable Long id) {
        return service.getByDefault(id);
    }

    @GetMapping("1/{id}")
    public Collection<TestEntity> getByCollate1(@PathVariable Long id) {
        return service.getByCollate1(id);
    }

    @GetMapping("2/{id}")
    public Collection<TestEntity> getByCollate2(@PathVariable Long id) {
        return service.getByCollate2(id);
    }

    @GetMapping("3/{id}")
    public Collection<TestEntity> getByCollate3(@PathVariable Long id) {
        return service.getByCollate3(id);
    }
}
