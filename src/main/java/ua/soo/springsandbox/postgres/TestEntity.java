package ua.soo.springsandbox.postgres;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class TestEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private String name;
    private LocalDate docDate;
    private LocalDateTime createdDate;
    private BigDecimal amount;

    public static TestEntity of(String code, String name, LocalDate docDate, BigDecimal amount) {
        final TestEntity entity = new TestEntity();
        entity.setCode(code);
        entity.setName(name);
        entity.setCreatedDate(LocalDateTime.now());
        entity.setDocDate(docDate);
        entity.setAmount(amount);
        return entity;
    }
}
