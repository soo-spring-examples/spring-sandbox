package ua.soo.springsandbox.postgres;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

import java.util.List;

@Slf4j
public class Collate3Function implements SQLFunction {
    @Override
    public boolean hasArguments() {
        return true;
    }

    @Override
    public boolean hasParenthesesIfNoArguments() {
        return false;
    }

    @Override
    public Type getReturnType(Type firstArgumentType, Mapping mapping) throws QueryException {
        return firstArgumentType;
    }

    @Override
    public String render(Type firstArgumentType, List arguments, SessionFactoryImplementor factory) throws QueryException {
        log.info("arguments = {}", arguments);

        String fieldName = (String) arguments.get(0);
        log.info("fieldName = {}", fieldName);

        String collate = (String) arguments.get(1);
        log.info("collate = {}", collate);

        String result = String.format("(%s COLLATE \"%s\")", fieldName, collate);
        log.info("result = {}", result);

        return result;
    }
}
