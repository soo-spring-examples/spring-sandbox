package ua.soo.springsandbox.postgres;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Component
@AllArgsConstructor
public class TestEntityGenerator implements CommandLineRunner {

    private static final TestEntity[] DATA = {
            TestEntity.of("EN:A", "AA", LocalDate.now(), BigDecimal.ZERO),
            TestEntity.of("EN:A", "Aa", LocalDate.now().minusDays(1), BigDecimal.valueOf(10000, 2)),
            TestEntity.of("EN:A", "aA", LocalDate.now().minusDays(2), BigDecimal.valueOf(20001, 2)),
            TestEntity.of("EN:A", "aa", LocalDate.now().minusDays(3), BigDecimal.valueOf(12002, 2)),
            TestEntity.of("EN:B", "BB", LocalDate.now().minusDays(4), BigDecimal.valueOf(30003, 2)),
            TestEntity.of("EN:B", "bb", LocalDate.now().minusDays(5), BigDecimal.valueOf(13041, 3)),
            TestEntity.of("UA:А", "АА", LocalDate.now().minusDays(10), BigDecimal.valueOf(40051, 3)),
            TestEntity.of("UA:А", "Аа", LocalDate.now().minusDays(11), BigDecimal.valueOf(14061, 3)),
            TestEntity.of("UA:А", "аА", LocalDate.now().minusDays(12), BigDecimal.valueOf(50071, 4)),
            TestEntity.of("UA:А", "аа", LocalDate.now().minusDays(13), BigDecimal.valueOf(15081, 4)),
            TestEntity.of("UA:Б", "ББ", LocalDate.now().minusDays(14), BigDecimal.valueOf(60091, 4)),
            TestEntity.of("UA:Б", "бб", LocalDate.now().minusDays(15), BigDecimal.valueOf(16000, 2))
    };

    private final TestEntityRepository repository;

    @Override
    public void run(String... args) {
        repository.saveAll(List.of(DATA));
    }
}
