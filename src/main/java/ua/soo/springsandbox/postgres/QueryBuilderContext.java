package ua.soo.springsandbox.postgres;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class QueryBuilderContext<T, R> {

    public final EntityManager em;
    public final Class<T> entityClass;
    public final Class<R> resultClass;
    public final CriteriaBuilder cb;
    public final CriteriaQuery<R> query;
    public final Root<T> root;

    public QueryBuilderContext(EntityManager em, Class<T> entityClass, Class<R> resultClass) {
        this.em = em;
        this.entityClass = entityClass;
        this.resultClass = resultClass;
        this.cb = em.getCriteriaBuilder();
        this.query = cb.createQuery(resultClass);
        this.root = query.from(entityClass);
    }
}
