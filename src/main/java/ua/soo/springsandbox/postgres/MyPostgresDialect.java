package ua.soo.springsandbox.postgres;

import org.hibernate.dialect.PostgreSQL10Dialect;
import org.hibernate.dialect.PostgreSQL95Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class MyPostgresDialect extends PostgreSQL95Dialect {

    public MyPostgresDialect() {
        super();

        registerFunction("collate1", new SQLFunctionTemplate(StandardBasicTypes.STRING, "(?1 COLLATE \"uk_UA.utf8\")"));
        registerFunction("collate2", new SQLFunctionTemplate(StandardBasicTypes.STRING, "(?1 COLLATE \"?2\")"));
        registerFunction("collate3", new Collate3Function());
    }
}
