package ua.soo.springsandbox.postgres;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class TestEntityService {

    private static final Predicate[] PREDICATES_EMPTY_ARRAY = {};

    @PersistenceContext
    private EntityManager em;

    private final TestEntityRepository repository;

    public TestEntityService(TestEntityRepository repository) {
        this.repository = repository;
    }

    public Collection<TestEntity> getByDefault(Long id) {
        return repository.findByIdGreaterThanOrderByName(id);
    }

    public Collection<TestEntity> getByCollate1(Long id) {
        final QueryBuilderContext<TestEntity, TestEntity> context = getQueryBuilderContext();
        final CriteriaBuilder cb = context.cb;

        final LinkedList<Predicate> predicates = new LinkedList<>();
        final ParameterExpression<Long> idParameter = cb.parameter(Long.class);
        predicates.add(cb.greaterThan(context.root.get("id"), idParameter));

        final List<Order> orders = new LinkedList<>();
        final Expression<String> expression1 = cb.function("collate1", String.class, context.root.get("name"));
        final Expression<String> expression2 = cb.function("collate1", String.class, context.root.get("code"));
        orders.add(cb.asc(expression1));
        orders.add(cb.desc(expression2));

        context.query
                .where(predicates.toArray(PREDICATES_EMPTY_ARRAY))
                .orderBy(orders);

        return em.createQuery(context.query)
                .setParameter(idParameter, id)
                .getResultList();
    }

    public Collection<TestEntity> getByCollate2(Long id) {
        return Collections.emptyList();
    }

    public Collection<TestEntity> getByCollate3(Long id) {
        return Collections.emptyList();
    }

    private QueryBuilderContext<TestEntity, TestEntity> getQueryBuilderContext() {
        return new QueryBuilderContext<>(em, TestEntity.class, TestEntity.class);
    }
}
